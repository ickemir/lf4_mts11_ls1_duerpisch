import math

x = float(input("bitte geben sie eine Zahl ein"))

if x <= 0:
    print("Sie sind im exponentiellen Bereich")
    print("Das Ergebnis ist ", math.exp(x))
if x > 0 and x <= 3:
    print("Sie sind im quadratischen Bereich")
    print("Das Ergebnis ist ", x*x+1)
if x > 3:
    print("Sie sind im linearen Bereich")
    print("Das Ergebnis ist ", 2*x+4)