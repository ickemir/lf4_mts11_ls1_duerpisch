package Fahrkartenautomat;
import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {
       Scanner tastatur = new Scanner(System.in);
      
       double zuZahlenderBetrag; 
       double eingezahlterGesamtbetrag;
       double eingeworfeneM�nze;
       double rueckgabebetrag;
       int anzahlKarten; // int da niemand halbe Karten kauft

       System.out.print("Zu zahlender Betrag (EURO): ");
       zuZahlenderBetrag = tastatur.nextDouble();
       System.out.print("Wieviele Fahrkarten sollen es sein?");
       anzahlKarten = tastatur.nextInt();
       zuZahlenderBetrag *= anzahlKarten;
       // die bereitsdeklarierte und belegte Variable zuZahlenderBetrag wird mit dem Produkt aus sich selbst und der Anzahl der karten belegt


       // Geldeinwurf
       // -----------
       eingezahlterGesamtbetrag = 0.0;
       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
       {
    	   System.out.printf("Noch zu zahlen : %.2f\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
    	   System.out.println("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
    	   eingeworfeneM�nze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneM�nze;
       }

       // Fahrscheinausgabe
       // -----------------
       System.out.println("\nFahrschein wird ausgegeben");
       for (int i = 0; i < 8; i++)
       {
          System.out.print("=");
          try {
			Thread.sleep(250);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       }
       System.out.println("\n\n");

       // rueckgeldberechnung und -Ausgabe
       // -------------------------------
       rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
       if(rueckgabebetrag > 0.0)
       {
    	   
    	   System.out.printf("Der rueckgabebetrag in H�he von : %.2f EURO wird in folgenden M�nzen ausgezahlt:\n", rueckgabebetrag);
    	   

           while(rueckgabebetrag >= 2.0) // 2 EURO-M�nzen
           {
        	  System.out.println("2 EURO");
	          rueckgabebetrag -= 2.0;
           }
           while(rueckgabebetrag >= 1.0) // 1 EURO-M�nzen
           {
        	  System.out.println("1 EURO");
	          rueckgabebetrag -= 1.0;
           }
           while(rueckgabebetrag >= 0.5) // 50 CENT-M�nzen
           {
        	  System.out.println("50 CENT");
	          rueckgabebetrag -= 0.5;
           }
           while(rueckgabebetrag >= 0.2) // 20 CENT-M�nzen
           {
        	  System.out.println("20 CENT");
 	          rueckgabebetrag -= 0.2;
           }
           while(rueckgabebetrag >= 0.1) // 10 CENT-M�nzen
           {
        	  System.out.println("10 CENT");
	          rueckgabebetrag -= 0.1;
           }
           while(rueckgabebetrag >= 0.05)// 5 CENT-M�nzen
           {
        	  System.out.println("5 CENT");
 	          rueckgabebetrag -= 0.05;
           }
       }

       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.");
    }
}