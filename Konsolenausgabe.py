print('Das ist ein “Beispielsatz“.')
print('Ein Beispielsatz ist das.')

x = "*"
a = 4
print("%7s" % x)
print("%8s" % (3*x))
print("%9s" % (5*x))
print("%10s" % (7*x))
print("%11s" % (9*x))
print("%12s" % (11*x))
print(13*x)
print("%8s" % (3*x))
print("%8s" % (3*x))
print("%8s" % (3*x))
print("%as" % (x))



a = 22.4234234
b = 111.2222
c = 4.0
d = 1000000.551
e = 97.34
print("%.2f" % a)
print("%.2f" % b)
print("%.2f" % c)
print("%.2f" % d)
print("%.2f" % e)
print("BLA")