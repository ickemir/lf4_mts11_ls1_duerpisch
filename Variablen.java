/*package eingabeAusgabe;*/

/** Variablen.java
    Ergaenzen Sie nach jedem Kommentar jeweils den Quellcode.
    @author Thomas Duerpisch
    @version 0.1
*/
public class Variablen {
  public static void main(String [] args){
    /* 1. Ein Zaehler soll die Programmdurchlaeufe zaehlen. Vereinbaren Sie eine geeignete Variable */
    int i = 1;

    /* 2. Weisen Sie dem Zaehler den Wert 25 zu und geben Sie ihn auf dem Bildschirm aus.*/
    i = 25;
    System.out.println(i);

    /* 3. Durch die Eingabe eines Buchstabens soll der Menuepunkt eines Programms ausgewaehlt werden. Vereinbaren Sie eine geeignete Variable */
    char menuAuswahl;

    /* 4. Weisen Sie dem Buchstaben den Wert 'C' zu
          und geben Sie ihn auf dem Bildschirm aus.*/
    menuAuswahl = 'C';
    System.out.println(menuAuswahl);

    /* 5. Fuer eine genaue astronomische Berechnung sind grosse Zahlenwerte
          notwendig.
          Vereinbaren Sie eine geeignete Variable */
    long grosseAstronomischeWerte;

    /* 6. Weisen Sie der Zahl den Wert der Lichtgeschwindigkeit zu
          und geben Sie sie auf dem Bildschirm aus.*/
    grosseAstronomischeWerte = 299792458;
    System.out.println(grosseAstronomischeWerte);

    /* 7. Sieben Personen gruenden einen Verein. Fuer eine Vereinsverwaltung
          soll die Anzahl der Mitglieder erfasst werden.
          Vereinbaren Sie eine geeignete Variable und initialisieren sie
          diese sinnvoll.*/
    byte mitgliederZahl = 7;

    /* 8. Geben Sie die Anzahl der Mitglieder auf dem Bildschirm aus.*/
    System.out.println(mitgliederZahl);

    /* 9. Fuer eine Berechnung wird die elektrische Elementarladung benoetigt.
          Vereinbaren Sie eine geeignete Variable und geben Sie sie auf
          dem Bildschirm aus.*/
    double elementarLadung = 0.0000000000000000001602176634;
    System.out.println(elementarLadung);

    /*10. Ein Buchhaltungsprogramm soll festhalten, ob eine Zahlung erfolgt ist.
          Vereinbaren Sie eine geeignete Variable. */
    boolean gezahlt;

    /*11. Die Zahlung ist erfolgt.
          Weisen Sie der Variable den entsprechenden Wert zu
          und geben Sie die Variable auf dem Bildschirm aus.*/
    gezahlt = true;
    System.out.println(gezahlt);

  }//main
}// Variablen