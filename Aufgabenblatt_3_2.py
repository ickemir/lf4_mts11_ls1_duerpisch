import math

def mittel (x,y):
    return (x+y)/2

def multi (x,y):
    return x * y

def wuerfel (x):
    return x * x * x

def quader (x,y,z):
    return x * y * z

def pyramide (x,h):
    return "{:.2f}".format((x*x*h)/3)

def kugel (r):
    return "{:.2f}".format((4/3)*wuerfel(r)*math.pi)

x = float(input("bitte geben sie eine zahl ein"))
y = float(input("bitte geben sie eine zweite zahl ein"))
z = float(input("bitte geben sie eine dritte zahl ein"))
h = float(input("bitte geben sie eine hoehe ein"))
r = float(input("bitte geben sie einen radius ein"))

print("Der Mittelwert der ersten beiden Zahlen ist ",mittel(x,y))
print("Das Produkt der ersten beiden Zahlen ist ", multi(x,y))
print("das Volumen des Wuerfels ist: ", wuerfel(x))
print("Das Volumen des Quaders ist: ", quader(x,y,z))
print("Das Volumen der Pyramide ist: ", pyramide(x,h))
print("Das Volumen der Kugel ist: ", kugel(r))



