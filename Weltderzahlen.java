/**
  *   Aufgabe:  Recherechieren Sie im Internet !
  * 
  *   Sie dürfen nicht die Namen der Variablen verändern !!!
  *
  *   Vergessen Sie nicht den richtigen Datentyp !!
  *
  *
  * @version 1.0 from 21.08.2019
  * @author << Ihr Name >>
  */

public class WeltDerZahlen {

  public static void main(String[] args) {
    
    /*  *********************************************************
    
         Zuerst werden die Variablen mit den Werten festgelegt!
    
    *********************************************************** */
    // Im Internet gefunden ?
    // Die Anzahl der Planeten in unserem Sonnesystem                    
    int anzahlPlaneten =  8;
    
    // Anzahl der Sterne in unserer Milchstraße
        long anzahlSterne = 400000000L;
    
    // Wie viele Einwohner hat Berlin?
        int bewohnerBerlin = 3645000;
    
    // Wie alt bist du?  Wie viele Tage sind das?
    
       short alterTage = 12892;
    
    // Wie viel wiegt das schwerste Tier der Welt?
    // Schreiben Sie das Gewicht in Kilogramm auf!
       int gewichtKilogramm = 150000;
    
    // Schreiben Sie auf, wie viele km² das größte Land er Erde hat?
       int flaecheGroessteLand = 17130000;
    
    // Wie groß ist das kleinste Land der Erde?
    
       float flaecheKleinsteLand = 0.44F;
    
    
    
    
    /*  *********************************************************
    
         Programmieren Sie jetzt die Ausgaben der Werte in den Variablen
    
    *********************************************************** */
    
    System.out.println("Anzhahl der Planeten: " + anzahlPlaneten);
    System.out.println("Anzahl der Sterne" + anzahlSterne);
    System.out.println("Berlin hat " + bewohnerBerlin + " Bewohner");
    System.out.println("Ich bin " + alterTage + " Tage alt");
    System.out.println("Ein Balwahl wiegt " + gewichtKilogramm);
    System.out.println("Russland ist mit einer Flaeche von " + flaecheGroessteLand + " das größte Land der Welt");
    System.out.println("Der Vatikandstaat ist mit einer Flaeche von " + flaecheKleinsteLand + "das kleinste Lan der Welt");
   
    System.out.println(" *******  Ende des Programms  ******* ");
    
  }
}
